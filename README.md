
# The Academy Web Application

This is a web application with features :
- Logging in with Discord (using Discord oauth2)
- Assigning membership type to each user
- Scheduled payment for members with chargeable membership type

## Frontend (ReactJS)

Go to the `frontend` directory folder using command :
  
### `cd frontend`

Then, run :

### `npm start`

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Backend (ASP .NET CORE)

Go to the `backend` directory folder using command :

### `cd backend`

Then, run :

### `dotnet watch run`

Open [http://localhost:5001](http://localhost:5001) to view it in the browser.

## Database (PostgreSQL)

Install `dotnet-ef` tool using command :

### `dotnet tool install -g dotnet-ef`

Go to the `backend` directory folder using command :

### `cd backend`

Add migrations using command :

### `dotnet-ef migrations add "message"`

Update database using command :

### `dotnet-ef database update`
